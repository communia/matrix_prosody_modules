-- Riot token authentication with fallback to jwt
-- based on token authentication by Atlassian

local log = module._log;
local host = module.host;
local st = require "util.stanza";
local is_admin = require "core.usermanager".is_admin;


local parentHostName = string.gmatch(tostring(host), "%w+.(%w.+)")();
if parentHostName == nil then
	log("error", "Failed to start - unable to get parent hostname");
	return;
end

local parentCtx = module:context(parentHostName);
if parentCtx == nil then
	log("error",
		"Failed to start - unable to get parent context for host: %s",
		tostring(parentHostName));
	return;
end

local token_util = module:require "token/util".new(parentCtx);

-- no token configuration
if token_util == nil then
    return;
end

log("info",
	"%s - starting MUC token verifier app_id: %s app_secret: %s allow empty: %s",
	tostring(host), tostring(token_util.appId), tostring(token_util.appSecret),
	tostring(token_util.allowEmptyToken));

local function verify_user(session, stanza)
	log("info", "Session token: %s, session room: %s",
		tostring(session.auth_token),
		tostring(session.jitsi_meet_room));

	-- token not required for admin users
	local user_jid = stanza.attr.from;
	if is_admin(user_jid) then
		log("debug", "Token not required from admin user: %s", user_jid);
		return nil;
	end

    log("info",
        "Will verify token for user: %s, room: %s ", user_jid, stanza.attr.to);
    if not token_util:verify_room(session, stanza.attr.to) then
        log("error", "Token %s not allowed to join: %s",
            tostring(session.auth_token), tostring(stanza.attr.to));
        session.send(
            st.error_reply(
                stanza, "cancel", "not-allowed", "Room and token mismatched"));
        return false; -- we need to just return non nil
    end
	log("info",
        "allowed: %s to enter/create room: %s", user_jid, stanza.attr.to);
end

local function verify_user_matrix(session, stanza)
	log("info", "Session token: %s, session room: %s, matrix token: %s",
		tostring(session.auth_token),
		tostring(session.jitsi_meet_room),
		tostring(session.matrix_token));

	-- token not required for admin users
	local user_jid = stanza.attr.from;
	if is_admin(user_jid) then
		log("info", "Token not required from admin user: %s", user_jid);
		return nil;
	end

    log("info",
        "Will verify token for user: %s, room: %s ", user_jid, stanza.attr.to);
    if not token_util:verify_room(session, stanza.attr.to) then
        log("error", "Token %s not allowed to join: %s",
            tostring(session.auth_token), tostring(stanza.attr.to));
        session.send(
            st.error_reply(
                stanza, "cancel", "not-allowed", "Room and token mismatched"));
        return false; -- we need to just return non nil
    end
	log("info",
        "allowed: %s to enter/create room: %s", user_jid, stanza.attr.to);
end

module:hook("muc-room-pre-create", function(event)
	local origin, stanza = event.origin, event.stanza;
	log("info", "pre create: %s %s", tostring(origin), tostring(stanza));
	if verify_user(origin, stanza) then
	  return true;
	end
	log("info", "pre create: could not log with jwt, continue with matrix")
	return verify_user_matrix(origin, stanza);
end);

module:hook("muc-occupant-pre-join", function(event)
	local origin, room, stanza = event.origin, event.room, event.stanza;
	log("info", "pre join: %s %s", tostring(room), tostring(stanza));
	if verify_user(origin, stanza) then
	  return true;
	end
	log("info", "pre join: could not log with jwt, continuing to matrix")
	return verify_user_matrix(origin, stanza);
end);
