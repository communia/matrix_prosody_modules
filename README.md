
These modules can minimally integrate a matrix network with prosody. They are 
made thinking with the pairing of jitsi <-> matrix (element im). By now just 
the authentication is satisfied. 

# Authenticate jitsi users in matrix

Using `mod_auth_token_matrix_fallback` :
It depends on a patched Element IM without the patch it will not work as Element 
IM needs to provide a special JWT where:
```
{
  "alg": "HS256",
  "typ": "JWT"
}
{
   iss: issuer,
   sub: homeserverUrl,
   matrix_id: userId,
   matrix_room: roomId,
   matrix_token: accessToken,
   matrix_url: homeserverUrl
}
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  'riot-not-secret'
)
```

With these properties `mod_auth_token_matrix_fallback` module will first try to
authenticate with standard mod_auth_token token(the one jitsi suggests in
https://github.com/jitsi/lib-jitsi-meet/blob/master/doc/tokens.md ) and if it
fails to authenticate then will try to login to `matrix_url` with the credentials
`matrix_id(userId):matrix_token(accessToken)` (without saving any user or token 
anywhere), and will carry on with some matrix checks:

- Check if `matrix_url` is in `matrix_accepted_servers` list in `/etc/prosody/conf.avail/yourjitsi.org.cfg.lua`.
- Check if `matrix_id(userId)` is in `matrix_room`.
- Check if `matrix_room(roomId)` has a jitsi widget with the current jitsi room id.
- Check if `matrix_id(userId)` can kick other users in `matrix_room`.

If all is ok then the user in matrix will be allowed to create the jitsi room
and will be an administrator of this room. Take care that (only for the matrix 
fallback case) secret must be the already configured jitsi token in 
`/etc/prosody/conf.avail/yourjitsi.org.cfg.lua` or the `matrix_secret` configured 
in `/etc/prosody/conf.avail/yourjitsi.org.cfg.lua`, for the `matrix_secret` it is
hardcoded in Element IM side as `riot-not-secret`(hardcoded because the browser 
webcrypto api cannot accept a blank secret to sign a token, while other api/libs 
do).

Others joined with the same method will also be jitsi room admins.
Others joined with suggested jitsi tokens as in https://github.com/jitsi/lib-jitsi-meet/blob/master/doc/tokens.md will act as admins also.
Others joined without any of the previous methods will be denied, unless the
room exists previously, then they can access as guests.


## Configure jitsi

Configure jitsi to work with jwt tokens as is said in https://github.com/jitsi/lib-jitsi-meet/blob/master/doc/tokens.md 
when you have it working follow these steps:

- Extract the tar.gz of this project to `/usr/share/jitsi-meet/prosody-plugins/`
- Or copy `token/matrix_util.lib.lua` 
to `/usr/share/jitsi-meet/prosody-plugins/token` and copy `mod_auth_token_matrix_fallback.lua` to 
`/usr/share/jitsi-meet/prosody-plugins/` and copy the `matrix` directory to `/usr/share/jitsi-meet/prosody-plugins/matrix/`.
- Install lua modules dependencies as http utils from prosody are useful only for xml:
```
luarocks install inspect
luarocks install http
luarocks install cjson
```
- Configure `/etc/prosody/conf.avail/yourjitsi.org.cfg.lua`: 
  - Add in global settings the accepted matrix servers:
  ```
  matrix_accepted_servers = {
    -- No trailing slash here
    "https://your.matrixurl.org:8448"
  }
  ```
  - Add the matrix_secret(as commented above it is hardcoded in element im as riot-not-secret:
  ```
  matrix_secret = "riot-not-secret"
  ```
  - Add The authentication method in each virtualhost:
  ```
  VirtualHost "yourjitsi.org"
    authentication = "token_matrix_fallback"
    ...
    -- Setting false will redirect to guest virtualhost
    allow_empty_token = false
    ...
  ```
  ```
  -- Guest virtualhost
  VirtualHost "guest.kunsido.communia.org"
    authentication = "token_matrix_fallback"
    -- In this case allow empty tokens as we want to allow 
    -- also guest users enter in existing rooms
    allow_empty_token = true
    ...
  ```

Then you can check in Element IM with a custom jitsi if everything is ok, adding a jitsi widget 
and entering there. If something fails you can check the prosody logs.


# Controlling users in MUC (multiuser conference)

The other modules in this project are in developing state.

## Matrix MUC user hooks
IT's in mod_matrix_token_with_jwt_fallback_verification 

It's not implemented by now, but ideally will hook to room-pre-create and 
pre-join to check things. 

To develop it add in muc configuration in `/etc/prosody/conf.avail/yourjitsi.org.cfg.lua`:

```
Component "conference.kunsido.communia.org" "muc"
 ...
 modules_enabled = {
   ...
   "matrix_token_with_jwt_fallback_verification";
   ...
 }
```

## Matrix test

Just to test things... 

When a message is post in jitsi chat: 
Will check whoami using the lua matrix api (modified from https://github.com/aperezdc/lua-matrix ).
Will inspect the room name and state. 
Will log to prosody logs the above.

To develop it add in muc configuration in `/etc/prosody/conf.avail/yourjitsi.org.cfg.lua`:

```
Component "conference.kunsido.communia.org" "muc"
 ...
 modules_enabled = {
   ...
   "matrix_test";
   ...
 }
```

If used as Application service (then it must be added in matrix homeserver as:
in synapse homeserver.yaml:
```
app_service_config_files:
  - /usr/local/matrix-jitsi/registration.yaml
```
then in /usr/local/matrix-jitsi/registration.yaml :
```
id: jitsi
as_token: tokenobtainedasinElement
hs_token: tokenThatwillbeusedwhenhomeserveractsinjitsi(notused)
namespaces:
    users:
      - exclusive: false
        regex: "@*"
    rooms: []
    aliases:
      - exclusive: false
        regex : "#*"
url: null
sender_localpart: jitsiservice
rate_limited: false
```
Then fill token in the prosody test module.

