-- Matrix Token authentication
-- Copyright (C) 2020 Communia

local have_async, async = pcall(require, "util.async");
local jwt = require "luajwtjitsi";
local json_safe = require "cjson.safe";
local matrix = module:require "matrix/matrix";

local inspect = require('inspect');

local http_timeout = 30;
local http_headers = {
    ["User-Agent"] = "Prosody ("..prosody.version.."; "..prosody.platform..")"
};

local Util = {}
Util.__index = Util

--- Constructs util class for token verifications.
-- Constructor that uses the passed module to extract all the
-- needed configurations.
-- If configuration is missing returns nil
-- @param module the module in which options to check for configs.
-- @return the new instance or nil
function Util.new(module)
    local self = setmetatable({}, Util)

    self.appId = module:get_option_string("app_id");
    self.allow_anonymous = module:get_option_boolean("matrix_allow_anonymous") or true;
    self.appSecret = ""

    if self.appId == nil then
        module:log("error", "'app_id' must not be empty");
        return nil;
    end

    --array of accepted issuers: by default only includes our appId
    -- self.acceptedIssuers = module:get_option_array('asap_accepted_issuers',{self.appId})
    -- TODO add matrix as issuer item in acceptedIssuers

    -- array of accepted matrix servers defaults to empty
    self.acceptedMatrixServers = module:get_option_array('matrix_accepted_servers',{})

    -- Not really used, but it can be useful in next features
    if not have_async then
        module:log("error", "requires a version of Prosody with util.async");
        return nil;
    end

    return self
end

-- Sets the matrix api with user and token
-- @param matrixUrl the Url to matrix
-- @param matrixId the matrix id to check
-- @param matrixToken the matrix token of the user to do the check
function Util:set_matrix_api(matrixUrl, matrixToken, matrixId)
    self.matrixApi = matrix.api(matrixUrl, matrixToken, matrixId)
end

-- Returns true if matrix id is allowd to be
-- moderator of the jitsi iframed in the matrix room
-- @param matrixUrl the Url to matrix
-- @param matrixId the matrix id to check
-- @param matrixToken the matrix token of the user to do the check
-- @param matrixRoom the matrix room to check
-- @param jitsiRoom the current jitsi room
-- @return bool determining if user is a moderator in room, and as such one can moderate jitsi
-- or nil if it is not 
function Util:matrixId_can_moderate(matrixUrl, matrixId, matrixToken, matrixRoom, jitsiRoom)
    -- We don't call unprotected methods as it can throw errors, let's
    -- protect, next is sugar of self.matrix_api:get_room_state(matrixRoom)
    success,result = pcall(self.matrixApi.get_room_state, self.matrixApi, matrixRoom)
    module:log("info", inspect(result))
    
    if success then
	-- Check if is the same jitsi
        if not self:check_jitsi_room(jitsiRoom, result) then
	  return nil, "The room in matrix and in jitsi are not the same";
	end
        -- Check if user can moderate
	if not self:check_moderator_level(matrixId, result) then
	  return nil, "User cannot be determined as moderator of this room so it cannot start it";
	end
	return true
    else
      return nil, result;
    end
end

-- Verifies the user is a room member
-- @param matrixId the matrix user
-- @param roomState the room state table
-- @return true if user is in room or nil and message
function Util:user_in_room(matrixId, roomState)
  log("info", inspect(roomState))
  for state_key,state in pairs(roomState) do
    if state.type == "m.room.member" then
      if state.state_key == matrixId then
	return true
      end
    end
  end
  return nil, "User is not in room"
end

-- Checks user moderator level (if can kick)
-- @param matrixId the matrix user
-- @param roomState the room state table
-- return bool about can moderate or not
function Util:check_moderator_level(matrixId, roomState)
  for state_key,state in pairs(roomState) do
      if state.type == "m.room.power_levels" then
        local required_level = state.content.kick
        local user_level = 0
        if self:user_in_room(matrixId, roomState) then
          user_level = state.content.users[matrixId] or state.content.users_default
        end
        log("info","Required level to kick is %s and %s has %s", required_level, matrixId, user_level)
        return user_level >= required_level
      end
  end
end

-- Checks power level (if can be in the jitsi room)
-- @param matrixId the matrix user
-- @param roomState the room state table
-- return bool about can access room or not
function Util:check_access_by_power_level(matrixId, roomState)
  for state_key,state in pairs(roomState) do
      if state.type == "m.room.power_levels" then
        local required_level = 0
        local user_level = self.allow_anonymous and 0 or -1
        if self:user_in_room(matrixId, roomState) then
          user_level = state.content.users[matrixId] or state.content.users_default
        end
        log("info","Required level to be in room is %s and %s has %s", required_level, matrixId, user_level)
        return user_level >= required_level
      end
  end
end


-- Checks if jitsi room is the same as in matrix
-- @param jitsiRoom the jitsi room id
-- @param roomState the room state table
-- return bool about if is the same or not
function Util:check_jitsi_room(jitsiRoom, roomState)
  for state_key,state in pairs(roomState) do 
      if state.type == "im.vector.modular.widgets" then
        log("info", "checking that jitsi host:%s and matrix widget host:%s matches", module:get_host(), state.content.data.domain)
        log("info", "checking that jitsi room:%s and matrix widget room:%s matches", jitsiRoom, state.content.data.conferenceId)
        return state.content.data.domain..state.content.data.conferenceId:upper() == module:get_host()..jitsiRoom:upper()
      end
  end
end

--- Verifies issuer part of token
-- @param 'iss' claim from the token to verify
-- @return nil and error string or true for accepted claim
function Util:verify_issuer(issClaim)
    for i, iss in ipairs(self.acceptedIssuers) do
        if issClaim == iss then
            --claim matches an accepted issuer so return success
            return true;
        end
    end
    --if issClaim not found in acceptedIssuers, fail claim
    return nil, "Invalid issuer ('iss' claim)";
end

--- Verifies if matrix server can be used with this jitsi
-- @param 'matrixServer' claim from the token to verify
-- @return nil and error string or true for accepted claim
function Util:verify_matrix_server(matrixServer)
    for i, server in ipairs(self.acceptedMatrixServers) do
        if matrixServer == server then
            --claim matches an accepted issuer so return success
            return true;
        end
    end
    --if issClaim not found in acceptedIssuers, fail claim
    return nil, "Invalid server ('matrix_server' claim)";
end


--- Verifies token
-- @param token the token to verify
-- @param secret the secret to use to verify token
-- @param roomName the jitsi room_name as requested by bosh
-- @return nil and error or the extracted claims from the token
function Util:verify_token(token, secret, roomName)
    -- TODO allow public keys also
    local claims, err = jwt.decode(token, secret, true);
    if claims == nil then
        return nil, err;
    end

    local alg = claims["alg"];
    if alg ~= nil and (alg == "none" or alg == "") then
        return nil, "'alg' claim must not be empty";
    end

    local issClaim = claims["iss"];
    if issClaim == nil then
        return nil, "'iss' claim is missing";
    end
    -- TODO merge matrix_url with iss?
    -- add origin of matrix as issuer (we need to deal with general use case of
    -- many different origin jitsis can be integrated with different matrix as it's 
    -- coming from riot ...
    --check the issuer against the accepted list
    -- local issCheck, issCheckErr = self:verify_issuer(issClaim);
    -- if issCheck == nil then
    --    return nil, issCheckErr;
    --end

    local matrixId = claims["matrix_id"];
    if matrixId == nil then
        return nil, "'matrix_id' is missing";
    end
    local matrixRoom = claims["matrix_room"];
    if matrixRoom == nil then
      return nil, "'matrix_room' is missing";
    end
    local matrixToken = claims["matrix_token"];
    if matrixToken == nil then
      return nil, "'matrix_token' is missing";
    end
    -- TODO obtain url from matrix referer as fallback... and so on check with user and token 
    local matrixUrl = claims["matrix_url"];
    if matrixUrl == nil then
      return nil, "'matrix_url' is missing";
    end
    -- we need to deal with general use case of
    -- many different origin jitsis can be integrated with different matrix as it's 
    -- coming from riot ...
    -- check the server against the accepted list
    local matrixServerCheck, matrixServerCheckErr = self:verify_matrix_server(matrixUrl);
    if matrixServerCheck == nil then
      return nil, matrixServerCheckErr;
    end

    -- self.application_service_token = module:get_option_string("matrix_application_service_token");
    -- self.as_userid = module:get_option_string("matrix_application_service_userid")
    self:set_matrix_api(matrixUrl, matrixToken, matrixId)

    local matrixCheck, matrixCheckErr = self:matrixId_can_moderate(matrixUrl, matrixId, matrixToken, matrixRoom, roomName);
    if matrixCheck == nil then
        return nil, matrixCheckErr;
    end

    return claims;
end

--- Verifies token and process needed values to be stored in the session.
-- Token is obtained from session.auth_token.
-- Stores in session the following values:
-- session.jitsi_meet_room - the room name value from the token
-- session.jitsi_meet_domain - the domain name value from the token
-- session.jitsi_meet_context_user - the user details from the token
-- session.jitsi_meet_context_group - the group value from the token
-- session.jitsi_meet_context_features - the features value from the token
-- @param session the current session
-- @return false and error
function Util:process_and_verify_token(session)
    if session.auth_token == nil then
        if self.allowEmptyToken then
            return true;
        else
            return false, "not-allowed", "token required";
        end
    end
    -- now verify the whole token
    local claims, msg;
    -- allow token unsigned (but simple web crypto doesn't... so accept the secret next)
    claims, msg = self:verify_token(session.auth_token, "", session.jitsi_bosh_query_room);
    if claims == nil then
      log("info", "Could not verify with empty token let's check with configured matrix_secret")
      claims, msg = self:verify_token(session.auth_token, module:get_option_string("matrix_secret"), session.jitsi_bosh_query_room);
    end

    if claims ~= nil then
        -- Binds room name to the session which is later checked on MUC join
        session.jitsi_meet_room = claims["room"];
        -- Binds domain name to the session
        session.jitsi_meet_domain = claims["sub"];

        -- Binds the user details to the session if available
        if claims["context"] ~= nil then
          if claims["context"]["user"] ~= nil then
            session.jitsi_meet_context_user = claims["context"]["user"];
          end

          if claims["context"]["group"] ~= nil then
            -- Binds any group details to the session
            session.jitsi_meet_context_group = claims["context"]["group"];
          end

          if claims["context"]["features"] ~= nil then
            -- Binds any features details to the session
            session.jitsi_meet_context_features = claims["context"]["features"];
          end
        end
        return true;
    else
        return false, "not-allowed", msg;
    end
end

return Util;
